﻿
namespace rslServer.Storage
{
    public class Widget
    {
        private string _sport = "volleyball";
        private string _name = "name";
        private string _type = "Counter";
        private string _field = "field";

        public string Sport { get => _sport; set => _sport = value.ToLower().Trim(); }
        public string Name { get => _name; set => _name = value.Trim(); }
        public string Type { get => _type; set => _type = value.Trim(); }
        public string Field { get => _field; set => _field = value.Trim(); }
        public string LowerName { get => _name.ToLower(); }

        public Widget(string sport, string name, string type, string field)
        {
            Sport = sport;
            Name = name;
            Type = type;
            Field = field;
        }
    }
}
