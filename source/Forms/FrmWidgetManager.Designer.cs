﻿namespace rslServer.Forms
{
    partial class FrmWidgetManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupSelectedWidget = new System.Windows.Forms.GroupBox();
            this.btnRemove = new System.Windows.Forms.Button();
            this.cmbWidgetType = new System.Windows.Forms.ComboBox();
            this.labelWidgetType = new System.Windows.Forms.Label();
            this.btnReset = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.txtField = new System.Windows.Forms.TextBox();
            this.lblSBField = new System.Windows.Forms.Label();
            this.txtWidgetName = new System.Windows.Forms.TextBox();
            this.lblXPNName = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.groupWidgets = new System.Windows.Forms.GroupBox();
            this.lstWidgets = new System.Windows.Forms.ListBox();
            this.ctxMenuStripWidget = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.duplicateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnAddNew = new System.Windows.Forms.Button();
            this.groupSelectedWidget.SuspendLayout();
            this.groupWidgets.SuspendLayout();
            this.ctxMenuStripWidget.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupSelectedWidget
            // 
            this.groupSelectedWidget.Controls.Add(this.btnRemove);
            this.groupSelectedWidget.Controls.Add(this.cmbWidgetType);
            this.groupSelectedWidget.Controls.Add(this.labelWidgetType);
            this.groupSelectedWidget.Controls.Add(this.btnReset);
            this.groupSelectedWidget.Controls.Add(this.btnSave);
            this.groupSelectedWidget.Controls.Add(this.txtField);
            this.groupSelectedWidget.Controls.Add(this.lblSBField);
            this.groupSelectedWidget.Controls.Add(this.txtWidgetName);
            this.groupSelectedWidget.Controls.Add(this.lblXPNName);
            this.groupSelectedWidget.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupSelectedWidget.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.groupSelectedWidget.Location = new System.Drawing.Point(165, 0);
            this.groupSelectedWidget.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupSelectedWidget.Name = "groupSelectedWidget";
            this.groupSelectedWidget.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupSelectedWidget.Size = new System.Drawing.Size(347, 235);
            this.groupSelectedWidget.TabIndex = 7;
            this.groupSelectedWidget.TabStop = false;
            this.groupSelectedWidget.Text = "Selected Widget";
            // 
            // btnRemove
            // 
            this.btnRemove.BackColor = System.Drawing.SystemColors.Control;
            this.btnRemove.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRemove.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnRemove.Location = new System.Drawing.Point(230, 182);
            this.btnRemove.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(109, 34);
            this.btnRemove.TabIndex = 9;
            this.btnRemove.Text = "Remove";
            this.btnRemove.UseVisualStyleBackColor = false;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // cmbWidgetType
            // 
            this.cmbWidgetType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbWidgetType.FormattingEnabled = true;
            this.cmbWidgetType.Items.AddRange(new object[] {
            ""});
            this.cmbWidgetType.Location = new System.Drawing.Point(197, 84);
            this.cmbWidgetType.Name = "cmbWidgetType";
            this.cmbWidgetType.Size = new System.Drawing.Size(129, 21);
            this.cmbWidgetType.TabIndex = 8;
            // 
            // labelWidgetType
            // 
            this.labelWidgetType.AutoSize = true;
            this.labelWidgetType.Location = new System.Drawing.Point(8, 84);
            this.labelWidgetType.Name = "labelWidgetType";
            this.labelWidgetType.Size = new System.Drawing.Size(104, 17);
            this.labelWidgetType.TabIndex = 7;
            this.labelWidgetType.Tag = "";
            this.labelWidgetType.Text = "Widget Type:";
            // 
            // btnReset
            // 
            this.btnReset.BackColor = System.Drawing.SystemColors.Control;
            this.btnReset.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReset.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnReset.Location = new System.Drawing.Point(139, 182);
            this.btnReset.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(71, 34);
            this.btnReset.TabIndex = 6;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = false;
            this.btnReset.Click += new System.EventHandler(this.BtnReset_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.SystemColors.Control;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnSave.Location = new System.Drawing.Point(7, 182);
            this.btnSave.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(109, 34);
            this.btnSave.TabIndex = 5;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // txtField
            // 
            this.txtField.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtField.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtField.Location = new System.Drawing.Point(197, 136);
            this.txtField.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtField.Name = "txtField";
            this.txtField.Size = new System.Drawing.Size(129, 20);
            this.txtField.TabIndex = 3;
            this.txtField.Tag = "";
            // 
            // lblSBField
            // 
            this.lblSBField.AutoSize = true;
            this.lblSBField.Location = new System.Drawing.Point(8, 135);
            this.lblSBField.Name = "lblSBField";
            this.lblSBField.Size = new System.Drawing.Size(142, 17);
            this.lblSBField.TabIndex = 2;
            this.lblSBField.Text = "ScoreBridge Field:";
            // 
            // txtWidgetName
            // 
            this.txtWidgetName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtWidgetName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWidgetName.Location = new System.Drawing.Point(197, 42);
            this.txtWidgetName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtWidgetName.Name = "txtWidgetName";
            this.txtWidgetName.Size = new System.Drawing.Size(131, 20);
            this.txtWidgetName.TabIndex = 1;
            this.txtWidgetName.Tag = "";
            // 
            // lblXPNName
            // 
            this.lblXPNName.AutoSize = true;
            this.lblXPNName.Location = new System.Drawing.Point(8, 41);
            this.lblXPNName.Name = "lblXPNName";
            this.lblXPNName.Size = new System.Drawing.Size(109, 17);
            this.lblXPNName.TabIndex = 0;
            this.lblXPNName.Tag = "";
            this.lblXPNName.Text = "Widget Name:";
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.SystemColors.Control;
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnCancel.Location = new System.Drawing.Point(176, 240);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(317, 31);
            this.btnCancel.TabIndex = 8;
            this.btnCancel.TabStop = false;
            this.btnCancel.Text = "Close";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // groupWidgets
            // 
            this.groupWidgets.Controls.Add(this.lstWidgets);
            this.groupWidgets.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupWidgets.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.groupWidgets.Location = new System.Drawing.Point(4, 0);
            this.groupWidgets.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupWidgets.Name = "groupWidgets";
            this.groupWidgets.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupWidgets.Size = new System.Drawing.Size(155, 235);
            this.groupWidgets.TabIndex = 9;
            this.groupWidgets.TabStop = false;
            this.groupWidgets.Text = "XPN Widgets";
            // 
            // lstWidgets
            // 
            this.lstWidgets.BackColor = System.Drawing.Color.DimGray;
            this.lstWidgets.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lstWidgets.ContextMenuStrip = this.ctxMenuStripWidget;
            this.lstWidgets.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstWidgets.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstWidgets.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lstWidgets.FormattingEnabled = true;
            this.lstWidgets.ItemHeight = 17;
            this.lstWidgets.Location = new System.Drawing.Point(3, 18);
            this.lstWidgets.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lstWidgets.Name = "lstWidgets";
            this.lstWidgets.Size = new System.Drawing.Size(149, 215);
            this.lstWidgets.TabIndex = 0;
            this.lstWidgets.SelectedIndexChanged += new System.EventHandler(this.LstWidgets_SelectedIndexChanged);
            // 
            // ctxMenuStripWidget
            // 
            this.ctxMenuStripWidget.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.ctxMenuStripWidget.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.duplicateToolStripMenuItem,
            this.removeToolStripMenuItem});
            this.ctxMenuStripWidget.Name = "ctxMenuStripWidget";
            this.ctxMenuStripWidget.Size = new System.Drawing.Size(125, 48);
            this.ctxMenuStripWidget.Opening += new System.ComponentModel.CancelEventHandler(this.CtxMenuStripWidget_Opening);
            // 
            // duplicateToolStripMenuItem
            // 
            this.duplicateToolStripMenuItem.Name = "duplicateToolStripMenuItem";
            this.duplicateToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.duplicateToolStripMenuItem.Text = "Duplicate";
            this.duplicateToolStripMenuItem.Click += new System.EventHandler(this.DuplicateToolStripMenuItem_Click);
            // 
            // removeToolStripMenuItem
            // 
            this.removeToolStripMenuItem.Name = "removeToolStripMenuItem";
            this.removeToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.removeToolStripMenuItem.Text = "Remove";
            this.removeToolStripMenuItem.Click += new System.EventHandler(this.RemoveToolStripMenuItem_Click);
            // 
            // btnAddNew
            // 
            this.btnAddNew.BackColor = System.Drawing.SystemColors.Control;
            this.btnAddNew.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddNew.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnAddNew.Location = new System.Drawing.Point(15, 240);
            this.btnAddNew.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAddNew.Name = "btnAddNew";
            this.btnAddNew.Size = new System.Drawing.Size(133, 31);
            this.btnAddNew.TabIndex = 10;
            this.btnAddNew.TabStop = false;
            this.btnAddNew.Text = "Add";
            this.btnAddNew.UseVisualStyleBackColor = false;
            this.btnAddNew.Click += new System.EventHandler(this.BtnAddNew_Click);
            // 
            // FrmWidgetManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DimGray;
            this.ClientSize = new System.Drawing.Size(532, 313);
            this.ControlBox = false;
            this.Controls.Add(this.btnAddNew);
            this.Controls.Add(this.groupWidgets);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.groupSelectedWidget);
            this.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "FrmWidgetManager";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Widget Manager";
            this.Shown += new System.EventHandler(this.FrmWidgetBB_Shown);
            this.groupSelectedWidget.ResumeLayout(false);
            this.groupSelectedWidget.PerformLayout();
            this.groupWidgets.ResumeLayout(false);
            this.ctxMenuStripWidget.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox groupSelectedWidget;
        private System.Windows.Forms.TextBox txtField;
        private System.Windows.Forms.Label lblSBField;
        private System.Windows.Forms.TextBox txtWidgetName;
        private System.Windows.Forms.Label lblXPNName;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.GroupBox groupWidgets;
        private System.Windows.Forms.ListBox lstWidgets;
        private System.Windows.Forms.Button btnAddNew;
        private System.Windows.Forms.ContextMenuStrip ctxMenuStripWidget;
        private System.Windows.Forms.ToolStripMenuItem removeToolStripMenuItem;
        private System.Windows.Forms.Label labelWidgetType;
        private System.Windows.Forms.ComboBox cmbWidgetType;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.ToolStripMenuItem duplicateToolStripMenuItem;
    }
}