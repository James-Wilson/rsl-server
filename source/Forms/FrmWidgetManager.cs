﻿using rslServer.Extensions;
using rslServer.Storage;
using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;

namespace rslServer.Forms
{
    public partial class FrmWidgetManager : Form
    {
        private readonly FrmServer _parent;
        private readonly string sportTitle = "volleyball";
        private Widget selectedWidget;

        public FrmWidgetManager(FrmServer parent, string sport = "volleyball")
        {
            InitializeComponent();

            _parent = parent;
            sportTitle = sport;
            selectedWidget = null;

            if (sport == "volleyball")
            {
                Text = "VolleyBall Widget Manager";
            } else if (sport == "basketball")
            {
                Text = "BasketBall Widget Manager";
            }


            cmbWidgetType.Items.Clear();
            cmbWidgetType.Items.Add("Counter");
            cmbWidgetType.Items.Add("Clock");
            cmbWidgetType.Items.Add("TextList");

            // Reset widget list
            UpdateWidgets();
        }

        private void FrmWidgetBB_Shown(object sender, EventArgs e)
        {
            // Keep the widget list updated
            UpdateWidgets();

            EnableSelectedWidget(false);
        }

        private void LstWidgets_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Load the selected widget
            SelectWidget();
        }

        private void CtxMenuStripWidget_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            // If no widget is selected, cancel the menu from opening
            if (lstWidgets.SelectedIndex == -1)
                e.Cancel = true;
        }

        private void BtnAddNew_Click(object sender, EventArgs e)
        {
            AddWidget();
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            // If no widget is selected, return
            if (selectedWidget == null) return;

            // If the widgetName was removed, don't save
            if (txtWidgetName.Text.Length == 0)
            {
                MessageBox.Show("You cannot save with an empty widgetName", "Failed to Save!",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Question);
                return;
            };

            string widgetType = cmbWidgetType.GetItemText(cmbWidgetType.SelectedItem).Trim();
            if (widgetType.Length == 0)
            {
                MessageBox.Show("You cannot save with an empty type", "Failed to Save!",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Question);
                return;
            };

            // If the password was removed, don't save
            if (txtField.Text.Length == 0)
            {
                MessageBox.Show("You cannot save with an empty password", "Failed to Save!",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Question);
                return;
            };

            selectedWidget = _parent.StorageSettings.EditWidget(selectedWidget, sportTitle, txtWidgetName.Text, widgetType, txtField.Text);
            _parent.StorageSettings.Save();

            int index = lstWidgets.SelectedIndex;
            if (index == -1) return;

            // Update the lstWidgets item widgetName
            lstWidgets.Items[index] = txtWidgetName.Text.Trim();
        }

        private void BtnReset_Click(object sender, EventArgs e)
        {
            // Reload the current selected widget
            SelectWidget();
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            RemoveWidget();
        }

        private void RemoveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RemoveWidget();
        }

        private void DuplicateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddWidget(selectedWidget);
        }

        private void AddWidget(Widget duplicate = null)
        {
            string _name = duplicate != null ? $"{duplicate.Name}{lstWidgets.Items.Count + 1}" : $"widget{lstWidgets.Items.Count + 1}";
            string _type = duplicate != null ? duplicate.Type : cmbWidgetType.GetItemText(cmbWidgetType.Items[0]).Trim();
            string _field = duplicate != null ? duplicate.Field : $"field{lstWidgets.Items.Count + 1}";

            // Setup new widget to add
            Widget _newWidget = new Widget(sportTitle, _name, _type, _field);

            // Try to add the new widget, if this fails return out
            if (!_parent.StorageSettings.AddWidget(sportTitle, _newWidget.Name, _newWidget.Type, _newWidget.Field)) return;

            int newIndex = lstWidgets.Items.Add(_newWidget.Name);

            if (newIndex == -1) return;
            lstWidgets.SelectedIndex = newIndex;
        }

        private void UpdateWidgets(bool resetSelected = true)
        {
            Settings storageSettings = _parent.StorageSettings;

            // Reset data
            if (resetSelected) ResetSelectedWidget();
            EnableSelectedWidget(false);
            lstWidgets.Items.Clear();

            foreach (Widget widget in storageSettings.GetSportWidets(sportTitle))
            {
                // Skip any null widget
                if (widget == null) continue;

                lstWidgets.Items.Add(widget.Name);
            }
        }

        private void RemoveWidget()
        {
            // Only continue if a selected widget is set
            if (selectedWidget == null) return;

            DialogResult result = MessageBox.Show("Are you sure?\nThis action cannot be undone!",
                                $"Remove Widget: {selectedWidget.Name}",
                                MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

            // If the widget selected no, return
            if (result == DialogResult.No) return;

            _parent.StorageSettings.RemoveWidget(sportTitle, selectedWidget.Name);
            _parent.StorageSettings.Save();

            // Get current selected index
            int index = lstWidgets.SelectedIndex;
            lstWidgets.Items.RemoveAt(index);

            // If  no other items exist, don't set selected again
            if (lstWidgets.Items.Count == 0)
            {
                ResetSelectedWidget();
                EnableSelectedWidget(false);
                return;
            }

            if (index > 0)
                lstWidgets.SelectedIndex = index - 1;
            else
                lstWidgets.SelectedIndex = index;
        }

        private void SelectWidget()
        {
            // If nothing is selected, return
            if (lstWidgets.SelectedIndex == -1)
            {
                ResetSelectedWidget();
                EnableSelectedWidget(false);
                return;
            }

            // Try and get an widget based off the selected item
            selectedWidget = _parent.StorageSettings.GetWidet(sportTitle, lstWidgets.SelectedItem.ToString());

            // If widget was not found, do nothing
            if (selectedWidget == null)
            {
                ResetSelectedWidget();
                EnableSelectedWidget(false);
                return;
            }

            // Fill in Selected Widget fields
            txtWidgetName.Text = selectedWidget.Name;
            // Set the widget type
            int typeIndex = cmbWidgetType.Items.IndexOf(selectedWidget.Type);
            if (typeIndex < 0)
                typeIndex = 0;
            cmbWidgetType.SelectedIndex = typeIndex;
            // Make sure the password is only added to a text box as astricts
            txtField.Text = selectedWidget.Field;


            EnableSelectedWidget(true);
        }

        private void ResetSelectedWidget()
        {
            txtWidgetName.Text = "";
            txtField.Text = "";
            cmbWidgetType.SelectedIndex = -1;
            selectedWidget = null;
            lstWidgets.SelectedIndex = -1;
        }

        private void EnableSelectedWidget(bool enable = false)
        {
            txtWidgetName.Enabled = enable;
            txtField.Enabled = enable;
            cmbWidgetType.Enabled = enable;
            btnSave.Enabled = enable;
            btnReset.Enabled = enable;
            btnRemove.Enabled = enable;
        }
    }
}
