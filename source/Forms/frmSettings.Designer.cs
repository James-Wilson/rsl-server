﻿namespace rslServer.Forms
{
    partial class FrmSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.groupGeneral = new System.Windows.Forms.GroupBox();
            this.chkStartup = new System.Windows.Forms.CheckBox();
            this.groupNetwork = new System.Windows.Forms.GroupBox();
            this.cmbIP = new System.Windows.Forms.ComboBox();
            this.numPort = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupScoreBridge = new System.Windows.Forms.GroupBox();
            this.txtScoreBridgeUrl = new System.Windows.Forms.TextBox();
            this.chkScoreBridgeSync = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.groupGeneral.SuspendLayout();
            this.groupNetwork.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numPort)).BeginInit();
            this.groupScoreBridge.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.SystemColors.Control;
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnCancel.Location = new System.Drawing.Point(239, 299);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(109, 34);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "Close";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.BtnClose_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.SystemColors.Control;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnSave.Location = new System.Drawing.Point(11, 299);
            this.btnSave.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(109, 34);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // groupGeneral
            // 
            this.groupGeneral.Controls.Add(this.chkStartup);
            this.groupGeneral.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupGeneral.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupGeneral.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.groupGeneral.Location = new System.Drawing.Point(0, 212);
            this.groupGeneral.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupGeneral.Name = "groupGeneral";
            this.groupGeneral.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupGeneral.Size = new System.Drawing.Size(371, 64);
            this.groupGeneral.TabIndex = 8;
            this.groupGeneral.TabStop = false;
            this.groupGeneral.Text = "General";
            // 
            // chkStartup
            // 
            this.chkStartup.AutoSize = true;
            this.chkStartup.Checked = true;
            this.chkStartup.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkStartup.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.chkStartup.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.chkStartup.Location = new System.Drawing.Point(17, 26);
            this.chkStartup.Margin = new System.Windows.Forms.Padding(4);
            this.chkStartup.Name = "chkStartup";
            this.chkStartup.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.chkStartup.Size = new System.Drawing.Size(166, 25);
            this.chkStartup.TabIndex = 5;
            this.chkStartup.Tag = "Run this pogram on system startup";
            this.chkStartup.Text = "Run on Startup";
            this.chkStartup.UseVisualStyleBackColor = true;
            // 
            // groupNetwork
            // 
            this.groupNetwork.Controls.Add(this.cmbIP);
            this.groupNetwork.Controls.Add(this.numPort);
            this.groupNetwork.Controls.Add(this.label2);
            this.groupNetwork.Controls.Add(this.label1);
            this.groupNetwork.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupNetwork.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupNetwork.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.groupNetwork.Location = new System.Drawing.Point(0, 95);
            this.groupNetwork.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupNetwork.Name = "groupNetwork";
            this.groupNetwork.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupNetwork.Size = new System.Drawing.Size(371, 117);
            this.groupNetwork.TabIndex = 6;
            this.groupNetwork.TabStop = false;
            this.groupNetwork.Text = "Network";
            // 
            // cmbIP
            // 
            this.cmbIP.FormattingEnabled = true;
            this.cmbIP.Location = new System.Drawing.Point(51, 26);
            this.cmbIP.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cmbIP.Name = "cmbIP";
            this.cmbIP.Size = new System.Drawing.Size(297, 28);
            this.cmbIP.TabIndex = 4;
            // 
            // numPort
            // 
            this.numPort.Location = new System.Drawing.Point(69, 75);
            this.numPort.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.numPort.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.numPort.Name = "numPort";
            this.numPort.Size = new System.Drawing.Size(280, 27);
            this.numPort.TabIndex = 3;
            this.numPort.Value = new decimal(new int[] {
            8181,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 75);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "Port:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 20);
            this.label1.TabIndex = 0;
            this.label1.Tag = "This is for external devices connecting. Do NOT change this unless you have a spe" +
    "cfic reason!";
            this.label1.Text = "IP:";
            // 
            // groupScoreBridge
            // 
            this.groupScoreBridge.Controls.Add(this.txtScoreBridgeUrl);
            this.groupScoreBridge.Controls.Add(this.chkScoreBridgeSync);
            this.groupScoreBridge.Controls.Add(this.label4);
            this.groupScoreBridge.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupScoreBridge.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupScoreBridge.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.groupScoreBridge.Location = new System.Drawing.Point(0, 0);
            this.groupScoreBridge.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupScoreBridge.Name = "groupScoreBridge";
            this.groupScoreBridge.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupScoreBridge.Size = new System.Drawing.Size(371, 95);
            this.groupScoreBridge.TabIndex = 7;
            this.groupScoreBridge.TabStop = false;
            this.groupScoreBridge.Text = "ScoreBridge";
            // 
            // txtScoreBridgeUrl
            // 
            this.txtScoreBridgeUrl.Location = new System.Drawing.Point(59, 27);
            this.txtScoreBridgeUrl.Name = "txtScoreBridgeUrl";
            this.txtScoreBridgeUrl.Size = new System.Drawing.Size(288, 27);
            this.txtScoreBridgeUrl.TabIndex = 7;
            // 
            // chkScoreBridgeSync
            // 
            this.chkScoreBridgeSync.AutoSize = true;
            this.chkScoreBridgeSync.Checked = true;
            this.chkScoreBridgeSync.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkScoreBridgeSync.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.chkScoreBridgeSync.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.chkScoreBridgeSync.Location = new System.Drawing.Point(17, 64);
            this.chkScoreBridgeSync.Margin = new System.Windows.Forms.Padding(4);
            this.chkScoreBridgeSync.Name = "chkScoreBridgeSync";
            this.chkScoreBridgeSync.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.chkScoreBridgeSync.Size = new System.Drawing.Size(241, 25);
            this.chkScoreBridgeSync.TabIndex = 6;
            this.chkScoreBridgeSync.Tag = "Run this pogram on system startup";
            this.chkScoreBridgeSync.Text = "Enable Sync On Launch";
            this.chkScoreBridgeSync.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 30);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 20);
            this.label4.TabIndex = 0;
            this.label4.Tag = "This is for external devices connecting. Do NOT change this unless you have a spe" +
    "cfic reason!";
            this.label4.Text = "Url:";
            // 
            // FrmSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DimGray;
            this.ClientSize = new System.Drawing.Size(371, 385);
            this.ControlBox = false;
            this.Controls.Add(this.groupGeneral);
            this.Controls.Add(this.groupNetwork);
            this.Controls.Add(this.groupScoreBridge);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmSettings";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Settings";
            this.groupGeneral.ResumeLayout(false);
            this.groupGeneral.PerformLayout();
            this.groupNetwork.ResumeLayout(false);
            this.groupNetwork.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numPort)).EndInit();
            this.groupScoreBridge.ResumeLayout(false);
            this.groupScoreBridge.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.GroupBox groupGeneral;
        private System.Windows.Forms.CheckBox chkStartup;
        private System.Windows.Forms.GroupBox groupNetwork;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numPort;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbIP;
        private System.Windows.Forms.GroupBox groupScoreBridge;
        private System.Windows.Forms.CheckBox chkScoreBridgeSync;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtScoreBridgeUrl;
    }
}