﻿using Newtonsoft.Json.Linq;
using rslServer.Storage;
using System;
using System.Collections.Generic;
using System.Net;
using System.Windows.Forms;

namespace rslServer.ScoreBridge
{

    public class WidgetTimer
    {
        public int Value { get; set; }
        public bool IsRunning { get; set; }

        public WidgetTimer(int value = 0, bool isRunning = false)
        {
            Value = value;
            IsRunning = isRunning;
        }
    }

    public class SB_Connector
    {
        private readonly FrmServer parent;
        private string url = "http://localhost/Output.json";
        private readonly Timer tmrFetch = new Timer();
        private readonly Dictionary<string, WidgetTimer> widgetTimers = new Dictionary<string, WidgetTimer>();

        public Boolean IsConnected { get; private set; } = false;


        public SB_Connector(FrmServer serverForm)
        {
            parent = serverForm;
        }

        ~SB_Connector()
        {
            Disconnect();
        }

        public void SetUrl(string address = null)
        {
            url = address;
        }


        public void Disconnect()
        {
            if (!IsConnected)
            {
                parent.SetScoreBridgeSyncText("Enable ScoreBridge Sync");
                return;
            };

            IsConnected = false;
            tmrFetch.Stop();
            parent.SetScoreBridgeSyncText("Enable ScoreBridge Sync");
            parent.AddMessage("ScoreBridge sync system has been disabled!", "ScoreBridge Disabled", "Incoming");
        }

        public void Connect()
        {
            if (IsConnected)
            {
                parent.SetScoreBridgeSyncText("Disable ScoreBridge Sync");
                return;
            };

            if (url == null)
            {
                MessageBox.Show("You need to fill in a ScoreBridge url under File -> Settings, first!");
                parent.AddMessage("ScoreBridge sync system failed to enable. You need to fill in a ScoreBridge url under File -> Settings, first!", "ScoreBridge Error", "Incoming");
                return;
            }

            IsConnected = true;
            tmrFetch.Tick += new EventHandler(TmrFetch_Tick);
            tmrFetch.Interval = 500; // in miliseconds
            tmrFetch.Start();
            parent.SetScoreBridgeSyncText("Disable ScoreBridge Sync");
            parent.AddMessage("ScoreBridge sync system has been enabled!", "ScoreBridge Enabled", "Incoming");
        }

        private void TmrFetch_Tick(object sender, EventArgs e)
        {
            try
            {
                if (tmrFetch.Interval == 60000)
                {
                    tmrFetch.Interval = 500;
                }
                // If the websocket server isn't started yet, ignore tick
                if (!parent.Server.IsListening)
                    return;

                string json = new WebClient().DownloadString(url);
                JObject message = JObject.Parse(json);
                JProperty messageProp = SB_Data.GetFirstProp(message);
                if (messageProp.Name == "" || !messageProp.Value.HasValues) return;

                JObject jsonData = SB_Data.GetTokenObject(messageProp.Value, messageProp.Name);
                Settings storageSettings = parent.StorageSettings;
                // If the data has no values, ignore tick
                if (!jsonData.HasValues) return;

                foreach (Widget widget in storageSettings.GetSportWidets(messageProp.Name))
                {
                    // Skip any null widget
                    if (widget == null) continue;

                    // Check if the widget field exists in the loaded data
                    JToken value;
                    if (jsonData.TryGetValue(widget.Field, out value))
                    {
                        switch(widget.Type)
                        {
                            case "Counter":
                                int counterValue;
                                if (Int32.TryParse(value.ToString(), out counterValue))
                                {
                                    parent.Server.XpnFunctions.EditCounterWidget(widget.Name, counterValue);
                                }
                                break;
                            case "Clock":
                                // Only update the clock value if something changed
                                WidgetTimer widgetTimer;
                                int clockValue;
                                if (Int32.TryParse(value.ToString(), out clockValue))
                                {
                                    if (!widgetTimers.TryGetValue(widget.Name, out widgetTimer))
                                    {
                                        widgetTimer = new WidgetTimer(clockValue);
                                        widgetTimers.Add(widget.Name, widgetTimer);
                                    }


                                    if (widgetTimer.Value != clockValue)
                                    {
                                        // Value of the clock has changed, so update the widget
                                        parent.Server.XpnFunctions.SetClockWidgetTimerValue(widget.Name, clockValue);

                                        // Widget timer changed but isn't running, so start the timer and update our local state
                                        if (!widgetTimer.IsRunning)
                                            widgetTimer.IsRunning = parent.Server.XpnFunctions.StartClockWidget(widget.Name);
                                    }
                                    else if (widgetTimer.IsRunning)
                                    {
                                        // Value hasn't changed but clock is running, so stop the widget and update our local state
                                        widgetTimer.IsRunning = parent.Server.XpnFunctions.StopClockWidget(widget.Name);
                                    }
                                }
                                break;
                            case "TextList":
                                if (parent.Server.XpnFunctions.ClearTextListWidget(widget.Name))
                                {
                                    if (parent.Server.XpnFunctions.AddTextListWidgeString(widget.Name, value.ToString()) != -1)
                                    {
                                        parent.Server.XpnFunctions.ResetTextListWidgetIndex(widget.Name);
                                    }
                                }
                                break;
                            default:
                                break;
                        }
                    }
                }

                parent.AddMessage(message, "ScoreBridge Update", "Incoming");
            }
            catch (Exception ex)
            {
                parent.AddMessage(ex.Message, "ScoreBridge Error", "Incoming");
                tmrFetch.Interval = 60000;
            }
        }
    }
}
