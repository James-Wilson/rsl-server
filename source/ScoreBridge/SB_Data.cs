﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Linq;

namespace rslServer.ScoreBridge
{
    public static class SB_Data
    {
        public static JProperty GetFirstProp(JObject obj)
        {
            return obj.Properties().FirstOrDefault();
        }

        public static IEnumerable<JProperty> GetProps(JObject obj)
        {
            return obj.Properties();
        }

        public static JObject GetTokenObject(JToken token, string defaultKey = "value")
        {
            if (token.Type == JTokenType.Object)
            {
                return (JObject)token;
            }
            else
            {
                return new JObject(new JProperty(defaultKey, token));
            }
        }

        public static JObject GetPropObject(JProperty prop, string defaultKey = "value")
        {
            JToken token = prop.Value;
            return GetTokenObject(token, defaultKey);
        }

        public static string GetSport(JObject obj)
        {
            JProperty prop = GetFirstProp(obj);
            if (prop.Name != "" && !prop.Value.HasValues)
                return prop.Name;
            return null;
        }

        public static JToken GetData(JObject obj)
        {
            JProperty prop = GetFirstProp(obj);
            if (prop.Name != "" && !prop.Value.HasValues)
                return prop.Value;
            return null;
        }

    }
}
